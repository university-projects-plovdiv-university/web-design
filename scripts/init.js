window.onload = () => {
    insertNavigation();
    insertFooter();
};

function insertNavigation() {
    document.getElementById('nav-container').innerHTML =
        `<nav class="topnav" id="nav">
        <a class="link" id="logo-link" href="index.html"><img class="logo" id="nav-logo" src="assets/img/logo.png"/></a>
        <div class="links-container">
            <a class="menu-link link" id="about-link" href="#about">
                <span>About</span>
            </a>
            <a class="menu-link link" id="menu-link" href="#specs">
                <span>Specs</span>
            </a>
            <a class="menu-link link" id="menu-link" href="#gallery">
                <span>Gallery</span>
            </a>
        </div>
        <a href="javascript:void(0);" class="menu-link icon" onclick="toggleNavigation()">
            <i class="fa fa-bars" id="toggler-icon"></i>
        </a>
    </nav>
    `;
}

function toggleNavigation() {
    document.getElementById("nav").classList.toggle('responsive');
    document.getElementById(_navToggler).classList.toggle('fa-bars');
    document.getElementById(_navToggler).classList.toggle('fa-times');
}

function insertFooter() {
    document.getElementById('footer-container').innerHTML =
        `<footer id="reservation">
        <div class="footer-content">
            <p class="footer-text">New cars are cool, but the old-school is cooler!</p>
            <p class="footer-text">Resources:</p>
            <div class="banner-controls">
                <a href="https://en.wikipedia.org/wiki/Audi_80" target="_blank">
                    <button class="footer-button">Wiki</button>
                </a>
                <a href="https://www.instagram.com/airmax_80/" target="_blank">
                    <button class="footer-button">Images</button>
                </a>
            </div>
        </div>
        <div class="copyright">
            <p>This website is made by 
                <a class="copyright-link" href="https://www.linkedin.com/in/deyan-peychev/" target="_blank">Deyan Peychev</a> 
                as a course project at Plovdiv University "Paisii Hilendarski" | All rights reserved.
            </p>
        </div>
    </footer>`;
}